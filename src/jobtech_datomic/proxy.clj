(ns jobtech-datomic.proxy
  (:require [jobtech-datomic.tools.ops.ssh :as ssh]
            [clojure.java.io :as io])
  (:import [java.io BufferedReader Writer]
           [java.net ServerSocket]
           [clojure.lang IDeref IBlockingDeref]
           [java.lang AutoCloseable]))

(defrecord Proxy [port process exit]
  IDeref
  (deref [_]
    @exit)
  IBlockingDeref
  (deref [_ timeout-ms timeout-val]
    (deref exit timeout-ms timeout-val))
  AutoCloseable
  (close [_]
    (.destroy ^Process process)))

(defmethod print-method Proxy [p ^Writer writer]
  (.write writer "#")
  (.write writer (.getName (class p)))
  (print-method (dissoc p :process :exit) writer))

(defn start!
  "Opens a SOCKS proxy to the access gateway in the system for Datomic client access

  Required args:
  :system       Datomic system

  Optional args:
  :access-type  :client or :analytics (defaults to :client)
  :port         Local forwarded port
  :profile      Named profile from AWS credentials file
  :region       AWS Region of the Datomic system
  :ssho         Parameters passed to SSH as -o (defaults to [\"IdentitiesOnly=yes\"])

  Blocks until proxy is opened and returns a Proxy record that can be used to:
  - stop the proxy (using `.close` or [[stop!]]);
  - await its exit (using [[deref]]);
  - lookup automatically assigned port (using `:port` key).
  Throws if proxy could not be opened (e.g. if port is already bound)"
  ([] (start! {}))
  ([k v & kvs] (start! (apply hash-map k v kvs)))
  ([opts]
   (let [port (or (:port opts)
                  (with-open [socket (doto (ServerSocket. 0)
                                       (.setReuseAddress true))]
                    (.getLocalPort socket)))
         process (ssh/access
                   (-> opts
                       (assoc :port port)
                       (update :ssho #(or % ["IdentitiesOnly=yes"]))
                       (update :access-type #(or % :client))))
         exit-future (future (.waitFor process))]
     (with-open [^BufferedReader r (io/reader (.getErrorStream process))]
       (loop []
         (when-let [line (.readLine r)]
           (case line
             "debug1: channel 1: new [port listener]"
             true

             "Could not request local forwarding."
             (do (.destroy process) (throw (ex-info line {})))

             (recur)))))
     (map->Proxy
       {:process process
        :port port
        :exit exit-future}))))

(defn stop!
  "Stop the proxy"
  [proxy]
  (.close ^AutoCloseable proxy))