# Datomic Cloud REPL Proxy

## Installation

Use clj git dependency for this repo, e.g.

```clojure
se.jobtechdev.datomic/proxy {:git/url "https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/datomic-cloud-repl-proxy.git"
                             :sha "..."}
```

## Usage

You can start/stop the proxies explicitly:

```clojure
(require '[jobtech-datomic.proxy :as dp])

;; Start:

(def test-proxy (dp/start! {:system "tax-test-v4" :port 8900}))

(def prod-proxy (dp/start! {:system "tax-prod-v4" :port 8901}))

;; ... access test/prod envs ...

;; Stop:

(dp/stop! test-proxy)
(dp/stop! prod-proxy)
```

Alternatively, you can use `with-open` for scoped access:
```clojure
(with-open [{:keys [port]} (dp/start! :system "tax-test-v4")]
  (let [client-config {:server-type :ion
                       :proxy-port port 
                       ...}]
    ...))
```

## License

Licensed under the Eclipse Public License, Version 2.0